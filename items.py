from azalspy.health.helpers import *

###################################################################################################

class Offensive_Server(BaseItem):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

class Offensive_Mobile(BaseItem):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

class Offensive_Device(BaseItem):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

###################################################################################################


